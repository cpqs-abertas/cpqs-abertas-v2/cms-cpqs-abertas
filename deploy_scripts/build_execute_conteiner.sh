HOST_ADDRESS=$1
echo $HOST_ADDRESS
echo Rodando script
echo Running command: scp
scp -o StrictHostKeyChecking=no /tmp/cpqs_abertas_cms_images.tar.gz ubuntu@$HOST_ADDRESS:/tmp/cpqs_abertas_cms_images.tar.gz

echo Running command: ssh
ssh -o StrictHostKeyChecking=no  ubuntu@$HOST_ADDRESS bash -c "'

echo Running command: docker stop
docker stop cms_container
docker rm cms_container
echo Running command: docker load
docker load < /tmp/cpqs_abertas_cms_images.tar.gz
echo Running command: removendo .tar
rm /tmp/cpqs_abertas_cms_images.tar.gz
echo Running command: git pull master
cd ~/cms-cpqs-abertas
git checkout main
git pull
echo Running command: docker build
docker build -t cpqs_abertas_cms:latest .
echo Running command: docker volume create
docker volume create db
echo Running command: docker run
docker run -it -d -p 80:1337 --name cms_container -v db:/opt/app/db cpqs_abertas_cms:latest

'"
