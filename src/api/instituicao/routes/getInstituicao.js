module.exports = {
  routes: [
    {
      method: "GET",
      path: "/instituicao/:nome",
      handler: "instituicao.getInstituicaoByNomeController",
      config: {
        auth: false,
      },
    },
  ],
};
