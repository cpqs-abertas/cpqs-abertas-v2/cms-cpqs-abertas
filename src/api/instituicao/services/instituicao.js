"use strict";

/**
 * instituicao service
 */

const { createCoreService } = require("@strapi/strapi").factories;

module.exports = createCoreService(
  "api::instituicao.instituicao",
  ({ strapi }) => ({
    async getInstituicaoByNomeService(nome) {
      try {
        let result = await strapi.db
          .query("api::instituicao.instituicao")
          .findOne({
            where: { nome: nome },
            populate: {
              menu: true,
              logo: {
                fields: ['url']
              },
              arrow: {
                fields: ['url']
              },
              Footer: true,
              graphs: {
                populate: {
                  lineChart: true,
                  map: true,
                  barChart: true,
                  worldCloud: true,
                }
              },
            }
          });
        const departments = await strapi
          .service("api::departamento.departamento")
          .getDepartamentsByInstitute(nome);
        let resultDepartments = {};
        departments.forEach((d) => {
          resultDepartments[d.key] = {
            name: d.name,
            color: d.color,
            chefe: d.chefe,
            viceChefe: d.viceChefe,
            history: d.history,
            borderColor: d.borderColor,
            borderWidth: d.borderWidth,
          };
        });
        result = { ...result, departments: resultDepartments };
        return result;
      } catch (err) {
        ctx.body = err;
      }
    },
  })
);
