"use strict";

/**
 * instituicao controller
 */

const { createCoreController } = require("@strapi/strapi").factories;

module.exports = createCoreController(
  "api::instituicao.instituicao",
  ({ strapi }) => ({
    async getInstituicaoByNomeController(ctx) {
      try {
        const { nome } = ctx.params;
        const data = await strapi
          .service("api::instituicao.instituicao")
          .getInstituicaoByNomeService(nome);

        return data;
      } catch (err) {
        ctx.body = err;
      }
    },
  })
);
