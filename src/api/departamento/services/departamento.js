"use strict";

/**
 * departamento service
 */

const { createCoreService } = require("@strapi/strapi").factories;

module.exports = createCoreService(
  "api::departamento.departamento",
  ({ strapi }) => ({
    async getDepartamentsByInstitute(nome) {
      try {
        const result = await strapi.db
          .query("api::departamento.departamento")
          .findMany({
            where: {
              instituicao: {
                nome: nome,
              },
            },
          });
        return result;
      } catch (err) {
        ctx.body = err;
      }
    },
  })
);
